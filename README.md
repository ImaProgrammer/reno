# What is Reno
Reno is a port of Zeno into Rust, due to an experiment to see if I can port
C/C++ code into Rust. <br />
Reno will implement other features that Zeno might not have to innovate and to
kinda act like a testing ground for features for main Zeno. <br />
If features work in here, I hope they would get implemented into Zeno to make it
a better game/virtual machine.

# What is the current state of the project
Currently it is in prototype stages.

# Who is making this
ImaProgrammer is the main developer and project leader.

# How do I run this
Reno/Zeno runs on a system of modules that expand the abilities of the
game/virtual machine to give it a better experience and a sense of modulation.
