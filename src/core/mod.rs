// Our use cases
use super::common;

pub struct RenoModule {
    data: usize,
    step: bool,
}

pub struct RenoCore {
    modules: RenoModule,
    memory: common::RenoCell,
    data: usize,
    step: bool,
    active: bool,
}

impl RenoCore {
    pub fn new() -> Self {
        RenoCore {
            modules: RenoModule::new(),
            memory: common::RenoCell::new(),
            data: 0,
            step: false,
            active: false,
        }
    }
}

impl RenoModule {
    pub fn new() -> Self {
        RenoModule {
            data: 0,
            step: false,
        }
    }

    pub fn module_set(&mut self, reno_core: RenoCore ) -> bool {
        true
    }
}
