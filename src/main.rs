// Our internal crates
pub mod common;
mod core;
// Our use cases
use std::*;

fn main() {
    // Gather our arguments from the command line
    let arguments: Vec<String> = env::args().collect();
    // Create a variable containing our core module
    let reno_core = core::RenoCore::new();

    // Check if our arguments aren't smaller or bigger than 3
    if arguments.len() != 3 {
        panic!("Usage: {} <module config> <executable>", arguments[0]);
    }
    
    // Check if we have loaded a module
    if load_module_config(&reno_core, &arguments[1]) == false {
        panic!("Error: could not load module config: {}", arguments[1]);
    }
    // Check if we have loaded a reno executable
    if load_executable(&reno_core, &arguments[2]) == false {
        panic!("Error: could not load executable: {}", arguments[2]);
    }

    release_all_handles(&reno_core);
}

fn release_all_handles(reno_core: &core::RenoCore) {

}

fn core_step(reno_core: &core::RenoCore) -> bool {
    true
}

fn load_module_config(reno_core: &core::RenoCore, filename: &String) -> bool {
    let file = fs::File::open(filename);
    
    true
}

fn load_executable(reno_core: &core::RenoCore, filename: &String) -> bool {
    true
}
