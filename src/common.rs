pub struct RenoCell {
    pub cell_size: usize,
    pub module_size: usize,
    pub core_size: usize,
    pub memory_size: usize,
    pub module_count: usize,
}

impl RenoCell {
    pub fn new() -> Self {
        RenoCell {
            cell_size: 0,
            module_size: 0,
            core_size: 0,
            memory_size: 0,
            module_count: 0,
        }
    }
}
